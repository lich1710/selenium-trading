# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 2
#     language: python
#     name: python2
# ---

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import numpy as np
import pandas as pd
import time
from datetime import date

# Declare webdriver used
driver = webdriver.Chrome("C:/bin/chromedriver.exe")

name = 'ACB'

driver.get("https://iboard.ssi.com.vn/bang-gia/" + name)


def get_spread_to_csv(name) :
    driver.get("https://iboard.ssi.com.vn/bang-gia/" + name)
    # Wait for page complete loading
    time.sleep(15)
    stock_table = driver.find_element_by_xpath("//tbody[@class='table-body' and @id='table-body-scroll']")
    stocks_data = stock_table.find_elements_by_xpath(".//tr")
    output_d = pd.DataFrame(columns=["CK","Tran","San","TC","Gia Mua 3","KL Mua 3","Gia Mua 2","KL Mua 2","Gia Mua 1","KL Mua 1","Gia","KL","%","Gia Ban 1","KL Ban 1","Gia Ban 2","KL Ban 2","Gia Ban 3","KL Ban 3","Cao","Thap","TB","KL","NN Mua","NN Ban","NN Du"])
    for index in range(len(stocks_data)):
        output_d.loc[len(output_d)] = ([u.get_attribute("textContent") for u in stocks_data[index].find_elements_by_tag_name("td")])
        print "Imported ", index+1, "/", len(stocks_data), stocks_data[index].find_elements_by_tag_name("td")[0].get_attribute("textContent")
    
    
    return output_d

# Get spread from HOSE
hose_df = get_spread_to_csv('hose')

# Get spread from HNX
hnx_df = get_spread_to_csv('hnx')

# +
outdir = './output'

hose_df.to_csv(path_or_buf=outdir+date.today().strftime("%d%m%Y")+"spread-hose.csv",index=False)
hnx_df.to_csv(path_or_buf=outdir+date.today().strftime("%d%m%Y")+"spread-hnx.csv",index=False)
# -


