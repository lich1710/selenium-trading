# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 2
#     language: python
#     name: python2
# ---

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def clear_form(form,default_value = ""):
    form.send_keys(Keys.END)
    while form.get_attribute('value') != default_value:
        form.send_keys(Keys.BACKSPACE)


# Declare webdriver used
driver = webdriver.Chrome()

driver.get("https://trade-as.vndirect.com.vn/chung-khoan/danh-muc")

# Check if there's new feature pop up
if "new-feature-popup" in driver.page_source:
    driver.find_element_by_xpath("//a[@class='closeBtn']").click()

# Click on Cơ Sở/Stock
stock_btn = driver.find_element_by_xpath('//div[@class="product"]//p[text()="Cơ sở"]')
driver.execute_script("arguments[0].click();", stock_btn)

# Perform login if not
if "login-popup" in driver.page_source:
    popup = driver.find_element_by_xpath('//div[@id="login-popup"]')
    username = raw_input("Username: ")
    password = raw_input("Password: ")

    user_form = popup.find_element_by_xpath('//input[@name="username"]')
    password_form = popup.find_element_by_xpath('//input[@name="password"]')
    remember_check_box = popup.find_element_by_xpath('//input[@name="remember-me"]')
    submit_button = popup.find_element_by_xpath('//button[@type="submit"]/span')
    
    # Clear the form
    user_form.clear()
    password_form.clear()
    
    # Fill the form
    user_form.send_keys(username)
    password_form.send_keys(password)
    
    # Check the Remember me box
    remember_check_box.click()
    
    # Click submit
    submit_button.click()

# Close overlay
if "skylight-close-button" in driver.page_source:
    driver.find_element_by_xpath('//a[@role="button" and @class="skylight-close-button"]').click()

# ## Giao Dich

horizontal_menu = driver.find_element_by_xpath('//div[@id="horizontal-menu"]/ul')

# Dat Lenh
giao_dich_dropdown = horizontal_menu.find_element_by_xpath('//*[contains(text(), "Giao dịch cổ phiếu")]')
giao_dich_dropdown.click()
driver.find_element_by_xpath("//*[contains(text(), 'Đặt lệnh')]").click()

if "VTOS" in driver.page_source:
    print "WARN: Need to fill in Login Token"

# Manual input data first
stock = "ACB"
volume = "100"
price = "22.5"

if "place-order-wrapper" in driver.page_source:
    wrapper = driver.find_element_by_xpath('//div[@class="place-order-wrapper"]')
    
    # Get form elements
    stock_form = wrapper.find_element_by_xpath('//input[@placeholder="Mã"]')
    volume_form = wrapper.find_element_by_xpath('//input[@placeholder="KL"]')
    price_form = wrapper.find_element_by_xpath('//input[@placeholder="Giá"]')
    
    # Clear data
    clear_form(stock_form)
    clear_form(volume_form,'0')
    clear_form(price_form)
    
    # Fill data
    stock_form.send_keys(stock)
    volume_form.send_keys(volume)
    price_form.send_keys(price)
    
    # Click button
    wrapper.find_element_by_xpath('//button/span/span[contains(text(), "MUA")]').click()
    
    # Check for error
    if "placeorder-msg errmsg" in driver.page_source:
        print "WARN: Failed to place order"


