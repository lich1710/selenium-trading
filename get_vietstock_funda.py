# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 2
#     language: python
#     name: python2
# ---

# +
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import numpy as np
import pandas as pd
import pickle
import h5py 
import time
from datetime import date
import unicodedata

from multiprocessing import Process
from numba import jit
# -

ticker = 'VNM'


def scrape_data(driver, period):
    content = driver.find_element_by_xpath("//div[@class='pos-relative content-grid w100']")
    tables = content.find_elements_by_xpath(".//table")

    final_df = pd.DataFrame()

    if len(tables) == 0:
        return final_df
    else:
        for loc in range(len(tables)):
            data = tables[loc].find_elements_by_xpath(".//tr")

            df_index = []
            df_matrix = []

            index = 0
            u = data[index].find_elements_by_tag_name("th")
            df_columns = []
            for ix in range(len(u)):
                if ix == 0:
                    continue
                u1 = u[ix]
                s = u1.get_attribute("textContent")
                s = unicodedata.normalize('NFKD', s).encode('ascii','ignore')
                if period == 1:
                    fiscal_period = int(s[4:8])
                elif period == 2:
                    fiscal_period = int(s[4]) + int(s[6:10]) * 100
                df_columns.append(fiscal_period)

            for index in range(1, len(data)):
                values = data[index].find_elements_by_tag_name("td")
                row = []
                # get index 
                s = values[0].get_attribute("textContent")
                s = unicodedata.normalize('NFKD', s).encode('ascii','ignore')
                df_index.append(s)

                for ix in range(-4, 0, 1):
                    v = values[ix].get_attribute("textContent")
                    v = unicodedata.normalize('NFKD', v).encode('ascii','ignore')
                    try:
                        row.append(float("".join(v.split(","))))
                    except:
                        row.append(np.nan)
                df_matrix.append(row)

            df = pd.DataFrame(data = df_matrix, columns = df_columns, index = df_index)

            if final_df.empty:
                final_df = df.copy()
            else:
                final_df = pd.concat([final_df, df])

    return final_df


# ### funda data

def get_funda_data(ticker, period = 2):
    # 1 is annual data, 2 is quarterly data
    
    try:
        driver = webdriver.Chrome("C:/bin/chromedriver")
        path = "https://finance.vietstock.vn/" + ticker + "/tai-chinh.htm"
        driver.get(path)
        time.sleep(3)
        element = driver.find_element_by_xpath("//select[@name='period']")
        select = Select(element)
        options = select.options
        if period == 1:
            option = options[0]
            option.click()
            submit_button = driver.find_elements_by_xpath("//button[@class='btn bg m-l']")[0]
            submit_button.click()
        elif period == 2:
            option = options[1]
            option.click()
            submit_button = driver.find_elements_by_xpath("//button[@class='btn bg m-l']")[0]
            submit_button.click()
        time.sleep(3)
    except:
        return pd.DataFrame()
    
    print 'Start scraping'
    
    final_df = pd.DataFrame()
    count = 0
    while (True):
        # scrape bctc
        df_1 = scrape_data(driver, period)

        # change to lc and scrape
        submit_button = driver.find_elements_by_xpath("//a[@href='?tab=LC']")[0]
        submit_button.click()
        time.sleep(3)
        df_2 = scrape_data(driver, period)

        df = pd.concat([df_1, df_2])
        
        new_cols = np.array(df.columns)
        if final_df.empty:
            final_df = df.copy()
        else:
            # index_1 = list(final_df.index)
            # index_2 = list(df.index)
            # full_index = list(set(index_1).union(set(index_2)))
            # final_df = final_df.reindex(full_index)
            # df = df.reindex(full_index)
            df = df.reindex(final_df.index)
            final_df = pd.concat([final_df, df], axis=1)
        
        # check to see if there is overlapped column
        # which means no more data to look back
        if count > 0:
            if np.sum(new_cols == old_cols) > 1:
                break
            old_cols = new_cols.copy()
        else:
            old_cols = new_cols.copy()
        
        count += 1
        
        # go to previous period
        submit_button = driver.find_elements_by_xpath("//a[@href='?tab=BCTT']")[0]
        submit_button.click()
        time.sleep(3)
        button = driver.find_elements_by_name("btn-page-2")[0]
        button.click()
        time.sleep(3)
        
    driver.close()
    
    final_df = final_df.loc[:,~final_df.columns.duplicated()]
    cols = final_df.columns.sort_values()
    final_df = final_df[cols]
    
    return final_df


get_funda_data('LINH', period = 1)

t = time.time()
df1 = get_funda_data(ticker, period = 1)
print 'Elapsed time: ', time.time() - t

df1.head()

df1.tail()

t = time.time()
df2 = get_funda_data(ticker, period = 2)
print 'Elapsed time: ', time.time() - t

df2.head()

df2.tail()


# ### guidance data

def get_guidance_data(ticker):
    try:
        driver = webdriver.Chrome("C:/bin/chromedriver")
        path = "https://finance.vietstock.vn/" + ticker + "/tai-chinh.htm"
        driver.get(path)
        time.sleep(3)
        element = driver.find_element_by_xpath("//select[@name='period']")
        select = Select(element)
        options = select.options
        if period == 1:
            option = options[0]
            option.click()
            submit_button = driver.find_elements_by_xpath("//button[@class='btn bg m-l']")[0]
            submit_button.click()
        elif period == 2:
            option = options[1]
            option.click()
            submit_button = driver.find_elements_by_xpath("//button[@class='btn bg m-l']")[0]
            submit_button.click()
        time.sleep(3)
    except:
        return pd.DataFrame()

    print 'Start scraping'
        
    final_df = pd.DataFrame()
    count = 0
    while(True):
        # change to ctkh and scrape
        submit_button = driver.find_elements_by_xpath("//a[@href='?tab=CTKH']")[0]
        submit_button.click()
        time.sleep(3)
        df = scrape_data(driver, period = 1)

        new_cols = np.array(df.columns)
        if final_df.empty:
            final_df = df.copy()
        else:
            # index_1 = list(final_df.index)
            # index_2 = list(df.index)
            # full_index = list(set(index_1).union(set(index_2)))
            # final_df = final_df.reindex(full_index)
            # df = df.reindex(full_index)
            df = df.reindex(final_df.index)
            final_df = pd.concat([final_df, df], axis=1)
        
        # check to see if there is overlapped column
        # which means no more data to look back
        if count > 0:
            if np.sum(new_cols == old_cols) > 1:
                break
            old_cols = new_cols.copy()
        else:
            old_cols = new_cols.copy()

        count += 1
        
        # go to previous period
        submit_button = driver.find_elements_by_xpath("//a[@href='?tab=BCTT']")[0]
        submit_button.click()
        time.sleep(3)
        button = driver.find_elements_by_name("btn-page-2")[0]
        button.click()
        time.sleep(3)
    
    driver.close()
    
    final_df = final_df.loc[:,~final_df.columns.duplicated()]
    cols = final_df.columns.sort_values()
    final_df = final_df[cols]
    
    return final_df


get_guidance_data('LINH')

t = time.time()
guidance_df = get_guidance_data(ticker)
print 'Elapsed time: ', time.time() - t

guidance_df.head()

guidance_df.tail()



# ### multi-processing

ticker_list = pickle.load(open("20200207_stock_list.p", "rb"))

len(ticker_list)

import datetime as dt
today = int(dt.datetime.strftime(dt.datetime.today(),"%Y%m%d"))

import os
directory = '%s'%today
if not os.path.exists(directory):
    os.mkdir(directory)


def get_data(proc_num, ticker_list):
    for ticker_index, ticker_name in enumerate(ticker_list):
        print '[PROC - %02d] %04d %s' % (proc_num, ticker_index, ticker_name)
        
        dump_path = '%s/%s' % (today, ticker_name)
        if not os.path.exists(dump_path):
            os.mkdir(dump_path)
        df = get_funda_data(ticker, period = 1)
        df.to_csv(dump_path + "/funda_y.csv")
        
        df = get_funda_data(ticker, period = 2)
        df.to_csv(dump_path + "/funda_q.csv")
        
        df = get_guidance_data(ticker)
        df.to_csv(dump_path + "/guid.csv")


t = time.time()
get_data(1, ['VNM','ACB'])
end_time = time.time()

print 'Time for dumping data: %.2f mins' %((end_time - start_time) / 60.)

test_ticker_list = ticker_list[:10]

# +
start_time = time.time()

proc_list = []

# Check the number of core in your server
max_process = 10

ticker_list_split = np.array_split(test_ticker_list, max_process)

for proc_num, ticker_list_single in enumerate(ticker_list_split):
    print '*****************', 'PROC-%02d, ticker counts: %d' % (proc_num, len(ticker_list_single)), '*****************'
    proc = Process(
        target=get_data,
        kwargs={'proc_num':proc_num,
                'ticker_list':ticker_list_single}
    )
    proc_list.append(proc)
    proc.start()

for proc in proc_list:
    proc.join()
    
end_time = time.time()    
# -

print 'Time for dumping data: %.2f mins' %((end_time - start_time) / 60.)




